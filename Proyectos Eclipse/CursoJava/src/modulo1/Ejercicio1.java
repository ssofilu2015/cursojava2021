package modulo1;

public class Ejercicio1 {

	public static void main(String[] args) {
		/*------------------------------------------------------------------------------*/
		/*seleccionas el paquete y le haces doble click en la pesta�a y agranda o achica 
		la pantalla
		Windows - Show View - Tasks (es para ver las tareas que alguien agreg�, o yo me 
		puedo dejar comentarios)*/
		
		System.out.println("Hola Mundo");
		/*------------------------------------------------------------------------------*/
		
		/*--------------------------------------------------------------*/
		/*Lineas de escape:
		 - \n es un enter
		 - \t es un espacio entre las palabras
		 - \\ hay que agregar doble barra para que reconozca una barra
		 - \" para que reconozca las comillas
		  */
		
		System.out.println("\n\nnombre");
		System.out.println("\\nombre\tapellido");
		System.out.println("\n\\nombre\t\\apellido");
		System.out.println("\n\\Sandra\t\\\"Sandra\"");
		
		/*--------------------------------------------------------------*/
		
		

	}

}
