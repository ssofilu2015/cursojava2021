package modulo2;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int i=0;
		int j=++i;
		
		System.out.println("j=" + j);
		System.out.println("i=" + i);
		
		System.out.println(i++ + "-" +  ++i);
		
		//desplazamientos
		
		i = 1357;
		j = i >> 5;
		
		System.out.println("El valor de " + i + " desplazado 5 lugares es " +  j);
		
		int x = -1357;
		int y = x >> 5;
		
		System.out.println("El valor de " + x + " desplazado 5 lugares es " +  y);
		
		

	}

}
