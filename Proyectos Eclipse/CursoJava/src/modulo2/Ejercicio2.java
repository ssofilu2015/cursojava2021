package modulo2;
/*Crear una clase identificada como Mod2_Ejercicio2.java dentro del paquete modulo2, recuerde 
tener chequeado el chekbox que permite crear el m�todo main, luego mostrar en pantalla todos
los tipos de datos enteros con sus correspondientes m�ximos y m�nimos, para ello, una variable 
para cada uno de los casos como se muestra a continuaci�n.
byte bmin = -128;
byte bmax = 127;
// reemplazar el 0 por el valor que corresponda en todos los caso
short smin = 0;
short smax = 0;
int imax = 0;
int imin = 0;
long lmin = 0;
long lmax = 0;
System.out.println("tipo\tminimo\tmaximo");
System.out.println("....\t......\t......");
System.out.println("\nbyte\t" + bmin + "\t" + bmax);
Y luego de ejecutarlo da el siguiente resultado:

Tipos de dato m�nimo m�ximo
---------------- ---------- -----------
byte -128 127
Colocar la siguiente informaci�n en forma de comentario en el ejercicio 2
Cu�l es la f�rmula general que me permite mostrar los m�nimos y los m�ximos teniendo en 
cuenta la cantidad de bits?
--------------------------------*/


public class Ejercicio2 {

	public static void main(String[] args) {
	
		byte bmin = -128;
		byte bmax = 127;
		short smin = 0;
		short smax = 0;
		int imax = 0;
		int imin = 0;
		long lmin = 0;
		long lmax = 0;
		
		smin = (short) ((Math.pow(2,15))*(-1));
	    smax = (short) (smin - 1); 
	    
	    imin = (int) ((Math.pow(2,31))*(-1));
	    imax = (int) (imin - 1);
	    
	    lmin = (long) ((Math.pow(2,63))*(-1));
	    lmax = (long) (lmin - 1);
		
		System.out.println("tipo\tminimo\t\t\tmaximo");
		System.out.println("....\t......\t\t\t......");
		System.out.println("\nbyte\t" + bmin + "\t\t\t" + bmax);
		System.out.println("\nshort\t" + smin + "\t\t\t" + smax);
		System.out.println("\nint\t" + imin + "\t\t" + imax);
		System.out.println("\nlong\t" + lmin + "\t" + lmax);
		
	   
	        
    
	}

}
