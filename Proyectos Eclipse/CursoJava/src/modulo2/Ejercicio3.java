package modulo2;

public class Ejercicio3 {

	public static void main(String[] args) {
	
/*4- Completar con el tipo de dato que corresponda, se debe tener en cuenta que la suma de 2 bytes
va a sobrepasar el byte.
byte b=10;
short s=20;
int i = 30;
long l= 40;
____ sumabb=b+b;
____ sumabs=b+s;
____ sumabi=b+i;
____ sumaii=i+i;
____ sumasl=s+l*/
		
		byte b=10;
		short s=20;
		int i = 30;
		long l= 40;
		
		short sumabb=(short) (b+b);	
		int sumabs=b+s;
		long sumabi=b+i;
		long sumasl=s+l;
		
/*5- Cual de las siguientes líneas dan errores de compilación y para esos casos cubrirlos con el casteo
correspondiente, asignándole un valor a las variables por ejemplo short s=25; int I = 1200, 
finalmente se deberán imprimir en pantalla.
b=s;
l=i;
b=i;
s=i*/
		
		
	
		b=(byte) s;
		l=i;
		b=(byte) i;
		s=(short) i;	

	System.out.println("\nLo que contiene b = " + b + "\nLo que contiene l = " + l + "\nLo que contiene b = " + b + "\nLo que contiene s = " + s);   
	}
}
