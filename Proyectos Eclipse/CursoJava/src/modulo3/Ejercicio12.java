package modulo3;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
/* 12. Realizar un sistema que permita asignar un numero dentro de una variable de tipo int y 
sabiendo que
a. La primer docena va de 1 a 12
b. La segunda docena va de 13 a 24
c. La tercer docena va de 25 a 36
Determinar a que docena pertenece el numero y si excediera estos limites informar el a trav�s 
del mensaje �el numero xxx esta fuera de rango�
Nota realizarlo con and y con or.*/
		
		int numero;
		
		System.out.println(" Ingrese un numero" );
		Scanner sc = new Scanner(System.in);
		numero = sc.nextInt();
		
		if(numero >=1 && numero <=12) {
			System.out.println(" El numero pertenece a la primer docena" );
		} else if(numero >=13 && numero <=24) {
			System.out.println(" El numero pertenece a la segunda docena" );
		} else if(numero >=25 && numero <=36) {
			System.out.println(" El numero pertenece a la tercer docena" );
		}else {
			System.out.println(" El numero " + numero + " esta fuera de rango" );
		}
		
	
		
	

	}

}
