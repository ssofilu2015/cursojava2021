package modulo3;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
/*14. Elaborar un sistema que permita realizar el ejercicio numero 5 a utilizando esta vez la sentencia 
switch. */

		
		int puesto;
		
		System.out.println(" Ingrese el puesto" );
		Scanner sc = new Scanner(System.in);
		puesto = sc.nextInt();
			
				
			
			switch (puesto) {
			 
			 case 1:
				 System.out.println("Obtiene la medalla de oro");
				 break;
			
			 case 2:
					System.out.println("Obtiene la medalla de plata");
				 break;
				 
			 case 3:
					System.out.println("Obtiene la medalla de bronce");
				 break;
				 
				 
			 default:
				 System.out.println("Siga participando");
				 break;
		 }
			
			
	}

}
