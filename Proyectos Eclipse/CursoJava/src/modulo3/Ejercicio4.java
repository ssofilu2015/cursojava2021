package modulo3;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
/*4. Realizar un programa que permita ingresar una categor�a las cuales pueden ser �a�, �b� o �c�, 
a. Si corresponde a esta categor�a mostrar en pantalla la palabra �hijo�
b. Si corresponde a esta categor�a mostrar en pantalla la palabra �padres�
c. Si corresponde a esta categor�a mostrar en pantalla la palabra �abuelos�.*/
		
		String categoria;
		
		System.out.println("Seleccione una de las opciones: ");
		System.out.println("a");
		System.out.println("b");
		System.out.println("c");
		Scanner sc = new Scanner (System.in);
		categoria = sc.nextLine();
		
		if (categoria.equals("a")) {
		System.out.println("La categoria a es: hijo");	
		} else if(categoria.equals("b")) {
			System.out.println("La categoria b es: padres");
		} else if(categoria.equals("c")) {
			System.out.println("La categoria c es: abuelos");
		} else {
			System.out.println("La categoria ingresada es incorrecta");
		}
	}

}
