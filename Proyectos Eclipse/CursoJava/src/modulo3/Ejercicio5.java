package modulo3;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
/*5. Realizar un programa que permita identificar con una variable de tipo entero el puesto que 
ocupa un torneo, existen 3 posiciones que son premiadas para los cuales deber� mostrar el 
siguiente mensaje en pantalla:
a. El primero obtiene la medalla de oro.
b. El segundo obtiene la medalla de plata.
c. Y el tercero obtiene la medalla de bronce.
d. Y para el resto siga participando.
If anidados*/
		
  int puesto = 4;
		
				
		if (puesto == 1) {
		System.out.println("Obtiene la medalla de oro");	
		} else if(puesto == 2) {
			System.out.println("Obtiene la medalla de plata");
		} else if(puesto == 3) {
			System.out.println("Obtiene la medalla de bronce");
		} else {
			System.out.println("Siga participando");
		}

	}

}
