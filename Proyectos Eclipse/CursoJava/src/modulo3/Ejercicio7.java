package modulo3;

public class Ejercicio7 {

	public static void main(String[] args) {
/* 7. Realizar un programa que permita la definición de 3 variables de tipo entera e imprimir la mayor
de todas.*/

		int primera = 15;
		int segunda = 10;
		int tercera = 22;
		
		if(primera > segunda && primera > tercera) {
			System.out.println("La mayor es la primera con: " + primera );
		} else if(segunda > tercera) {
			System.out.println("La mayor es la segunda con: " + segunda );
		} else {
			System.out.println("La mayor es la tercera con: " + tercera );
		}
		
	}

}
