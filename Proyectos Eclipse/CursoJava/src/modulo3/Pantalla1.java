package modulo3;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;

public class Pantalla1 {

	private JFrame frame;
	private JTextField textNota1;
	private JTextField textNota2;
	private JTextField textNota3;
	private JLabel lblResultado;
	private JLabel lblFigura;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla1 window = new Pantalla1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 533, 371);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPromedioDeNotas = new JLabel("Promedio de notas");
		lblPromedioDeNotas.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblPromedioDeNotas.setBounds(120, 23, 224, 29);
		frame.getContentPane().add(lblPromedioDeNotas);
		
		JLabel lblNota = new JLabel("nota 1");
		lblNota.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNota.setBounds(36, 113, 68, 20);
		frame.getContentPane().add(lblNota);
		
		JLabel lblNota_3 = new JLabel("nota 2");
		lblNota_3.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNota_3.setBounds(36, 144, 68, 20);
		frame.getContentPane().add(lblNota_3);
		
		JLabel lblNota_1 = new JLabel("nota 3");
		lblNota_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNota_1.setBounds(36, 175, 68, 20);
		frame.getContentPane().add(lblNota_1);
		
		textNota1 = new JTextField();
		textNota1.setFont(new Font("Tahoma", Font.ITALIC, 16));
		textNota1.setBounds(112, 115, 86, 20);
		frame.getContentPane().add(textNota1);
		textNota1.setColumns(10);
		
		textNota2 = new JTextField();
		textNota2.setFont(new Font("Tahoma", Font.ITALIC, 16));
		textNota2.setColumns(10);
		textNota2.setBounds(112, 146, 86, 20);
		frame.getContentPane().add(textNota2);
		
		textNota3 = new JTextField();
		textNota3.setFont(new Font("Tahoma", Font.ITALIC, 16));
		textNota3.setColumns(10);
		textNota3.setBounds(112, 177, 86, 20);
		frame.getContentPane().add(textNota3);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				float nota1 = Float.parseFloat(textNota1.getText());
				float nota2 = Float.parseFloat(textNota2.getText());
				float nota3 = Float.parseFloat(textNota3.getText());
				float promedio =(nota1+nota2+nota3)/3;
				lblResultado.setText(Float.toString(promedio));
				if(promedio>=7){
					lblResultado.setBackground(Color.GREEN);
					lblFigura.setIcon(new ImageIcon(Pantalla1.class.getResource("/iconos/Gano_32px.png")));
				}
					
				else{
					lblResultado.setBackground(Color.RED);
					lblFigura.setIcon(new ImageIcon(Pantalla1.class.getResource("/iconos/fantasma_32px.png")));
				}
					
			}
		});
		btnCalcular.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btnCalcular.setBounds(184, 208, 101, 29);
		frame.getContentPane().add(btnCalcular);
		
		JLabel lblPromedio = new JLabel("Promedio");
		lblPromedio.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblPromedio.setBounds(270, 82, 101, 29);
		frame.getContentPane().add(lblPromedio);
		
		lblResultado = new JLabel("");
		lblResultado.setBackground(Color.MAGENTA);
		lblResultado.setOpaque(true);
		lblResultado.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		lblResultado.setBounds(250, 149, 101, 25);
		frame.getContentPane().add(lblResultado);
		
		lblFigura = new JLabel("");		
		lblFigura.setBounds(400, 135, 74, 75);
		frame.getContentPane().add(lblFigura);
	}
}
