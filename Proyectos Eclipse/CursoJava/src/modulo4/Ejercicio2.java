package modulo4;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
	
/* 1) Realizar un programa que permita mostrar en pantalla la tabla de multiplicar de un valor 
ingresado a trav�s de una variable.*/
		
		System.out.println("Ingrese un numero ");
		Scanner sc = new Scanner(System.in);
		int numero = sc.nextInt();
		
		int suma = 0;
		int calculo = 0;
		int acu = 0;
	
		
		for(int i = 1; i<10; i++) {
			int multiplicar = numero * i;
			System.out.println(numero + " x " + i + " = " + multiplicar);
			
			//para el ejercicio dos con if
			if(multiplicar%2==0) {
				suma += multiplicar;
			}
			
			//para el ejercicio dos sin if
			calculo = multiplicar % 2;
			boolean cero = calculo == 0;
			
			while (cero == true) {
				
				acu+=multiplicar;
				cero = false;
					
			}
			
		}
		
		
		System.out.println("---------------------------------------------");
		System.out.println("Ejercicio 2");
		System.out.println("---------------------------------------------");
		
/* 2) Con relaci�n al punto 16 mostrar la suma de los valores pares.
a. Con if
b. Sin if, a trav�s de un algoritmo.*/
	
		
		System.out.println("a) La suma de los valores pares es = " + suma);	
		System.out.println("b) La suma de los valores pares es = " + acu);	
		

	}

}
