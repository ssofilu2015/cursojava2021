package modulo4;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
/* 3)Realizar dos ciclos for anidados de manera de mostrar todas las tablas*/
				
						
				for(int j = 1; j<10; j++) {
				for(int i = 1; i<10; i++) {
					int multiplicar = j * i;
					System.out.println(j + " x " + i + " = " + multiplicar);

				}

			}
	}
}
