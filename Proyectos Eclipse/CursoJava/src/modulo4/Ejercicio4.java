package modulo4;

public class Ejercicio4 {

	public static void main(String[] args) {
		
/* 4) Realizar un ciclo while que muestre 10 n�meros al azar 
 informando su suma y su promedio	*/
 
	
	    //va a tirar numeros aleatorios entre 0 y 30.
		int numero = (int) (Math.random()*30);
		int cont = 0;
		float suma = 0;
		float promedio = 0;

		System.out.println("Los numeros son : ");
		
		
		while (cont < 10) {
		
			suma += numero;
			cont++;
			System.out.println(numero);
			numero = (int) (Math.random()*30);
						
		}
		
		System.out.println("La suma total de los numeros es : " + suma);
		
		promedio = suma / cont;
		System.out.println("El promedio de los numeros es : " + promedio);
		
	}

}
