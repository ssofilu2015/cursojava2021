package modulo4;

public class Ejercicio5 {

	public static void main(String[] args) {
/* 5) Realizar un ciclo do while que muestre 10 n�meros al azar (Math.random())
  informando el m�ximo y el m�nimo de ellos.*/

		
		int numero = (int) (Math.random()*30);
		int cont = 0;
		int maximo;
		int minimo;
		
		maximo = numero;
		minimo = numero;

		System.out.println("Los numeros son : ");
		
		do {
			
		cont++;
		
		if(numero > maximo) {
			maximo = numero;
		}	
		
		if(numero < minimo) {
			minimo = numero;
		}	
		
		System.out.println(numero);
		numero = (int) (Math.random()*30);	
				
		}while (cont < 10);
		
	
		
		System.out.println("El maximo de los numeros es : " + maximo);
		System.out.println("El minimo de los numeros es : " + minimo);

	}

}
