package modulo4;

import java.util.Scanner;

public class Ejercicio6 {

	public static void main(String[] args) {
		
/* 6) De un empleado se conoce:
 		a. la categor�a: A,B,C
		b. la antig�edad
		c. el sueldo.
		Se quiere determinar el sueldo neto sabiendo que si la antig�edad esta entre
		1 y 5 a�os se le aumentan un 5% al sueldo bruto.
		6 y 10 a�os se le aumenta un 10% al sueldo bruto.
		Mas de 10 un 30%
		 Y un plus por categor�a
		A= 1000, B=2000, c=3000*/

		System.out.println("Ingrese la categor�a ('a', 'b' o 'c') ");
		Scanner sc = new Scanner(System.in);
		char categoria = sc.next().charAt(0);
		System.out.println("Ingrese la antiguedad ");
		sc = new Scanner(System.in);
		int antiguedad = sc.nextInt();
		System.out.println("Ingrese el sueldo bruto");
		sc = new Scanner(System.in);
		float bruto = sc.nextFloat();
		
		
		float neto = bruto;
		
		if(antiguedad >= 1 && antiguedad <= 5) {
			neto += bruto * 0.05;
	
		} else if(antiguedad >= 6 && antiguedad <= 10) {
			neto += bruto * 0.10;
		} else {
			neto += bruto * 0.30;
		}
		
		switch (categoria) {
		
		case 'a': 
			neto += 1000;
		break;
		
		case 'b':
			neto += 2000;
			break;
			
		case 'c':
			neto += 3000;
			break;
			
		default: 
			System.out.println("La categor�a ingresada es incorrecta ");
			break;
		}
		
		System.out.println("El sueldo del empleado es : $" + neto );
		
	}

}
