package modulo4;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.AbstractListModel;

public class PantallaConArray {

	private JFrame frame;
	private JComboBox comboBox;
	private JList listResult;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaConArray window = new PantallaConArray();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaConArray() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.getContentPane().setForeground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas de Multiplicar");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblNewLabel.setBounds(121, 30, 216, 22);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Tabla de");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		lblNewLabel_1.setBounds(121, 88, 109, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		comboBox = new JComboBox();
		//DEFINI UN aRRAY DE sTRING
		String strTablas[] = new String [10];
		for(int i=0;i<strTablas.length;i++)
			strTablas[i]= Integer.toString(i);
		comboBox.setModel(new DefaultComboBoxModel(strTablas));
		comboBox.setBounds(240, 86, 54, 22);
		frame.getContentPane().add(comboBox);
		

				
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(96, 113, 100, 122);
				frame.getContentPane().add(scrollPane);
		
				//cuando se crea el JList se hace click derecho sobre List y toco la opci�n Surround with, me genera JscrollPana
				//que seria para poder realizar Scroll sobre esa pantalla, un detalle gr�fico
				listResult = new JList();
				scrollPane.setViewportView(listResult);
				
				/*listResult.setModel(new AbstractListModel() {
					String[] values = new String[] {"2X0=0", "2X1=2", "2X3=6"};
					public int getSize() {
						return values.length;
					}
					public Object getElementAt(int index) {
						return values[index];
					}
				});*/
				
				JButton btnNewButton = new JButton("CALCULAR");
				btnNewButton.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
				 btnNewButton.addActionListener(new ActionListener(){
					 public void actionPerformed(ActionEvent arg0) {
						 int tabla = comboBox.getSelectedIndex();
						 System.out.println("eligio el " + tabla);
						 
						 String[] values = new String[10];
						 for(int i=0;i<10;i++)
							 //aca se hace el calculo para mostrar por pantalla
							 //ej, 2X1=2
						   values [i] = tabla + "x" + i + "=" + (i*tabla);	 
						 
						 						 
						 
							//listResult.add(comp)
							//Este es el codigo recupedo del plug in
							listResult.setModel(new AbstractListModel() {				
								public int getSize() {
									return values.length;
								}
								public Object getElementAt(int index) {
									return values[index];
								}
							});
							
						}
					});
				btnNewButton.setBounds(240, 165, 109, 22);
				frame.getContentPane().add(btnNewButton);
	}
}
