package modulo5;

public class Ejercicio1 {

	public static void main(String[] args) {

/* 1) Realizar un programa que cree un objeto String con el contenido �Hola mundo� y 
 mostrar:
a. El texto todo en may�sculas
b. El texto todo en min�sculas
c. Reemplazar la letra �o� por el n�mero 2.*/

		String hola = "Hola mundo";
		
		System.out.println("Todo en mayuscula = " + hola.toUpperCase());
		System.out.println("Todo en minuscula = " + hola.toLowerCase());
		System.out.println("Reemplazo de la letra o por el numero 2 = "
		+ hola.replace('o', '2'));

	}

}
