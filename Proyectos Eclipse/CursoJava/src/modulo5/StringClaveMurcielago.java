package modulo5;

import java.util.Scanner;

public class StringClaveMurcielago {

	public static void main(String[] args) {

		//opciones de String
		/*-charAt(le envio una posicion y me devuelve un caracter)
		 * -indexof(le envio un caracter y me devuelve una posicion)
		 * -replace(char old, char new)
		 * - toUpperCase (pasa todo a mayuscula)
		 * -toLowerCase
		 * */
		
		System.out.println("Ingrese un texto a codificar en clave murcielago");
		Scanner sc = new Scanner (System.in);
		//MURCIELAGO
		//0123456789
		String strTextoOriginal = sc.nextLine();
		System.out.println("Texto original = " + strTextoOriginal);
		System.out.println("Todo en mayuscula = " + strTextoOriginal.toUpperCase());
		System.out.println("Todo en minuscula = " + strTextoOriginal.toLowerCase());
				
						
		//strTextoOriginal(es la referencia a ese objeto y el cntenido no se puede modificar)
		//si por ejemplo quiere pasar todo a mayuscula con toUpperCase, toma la referencia
		//ese objeto y me devuelve otro pasado a mayuscula-
	
		System.out.println("---------------------------------");
		//sysout + control + espacio
		System.out.println("\n\n\ny la clave mmurcielago para cuando");
		
		//llamado on the fly
		//significa llamar a objetos no referenciados
		String strMurcielago = strTextoOriginal.toLowerCase() //El perro Verde
													.replace('m', '0')//el perro verde
													.replace('u', '1')//el perro verde
													.replace('r', '2')//el pe22o ve2de
													.replace('c', '3')//el pe22o ve2de
													.replace('i', '4')//el pe22o ve2de
													.replace('e', '5')//5l p522o v52d5
													.replace('l', '6')//56 p522o v52d5
													.replace('a', '7')//56 p522o v52d5
													.replace('g', '8')//56 p522o v52d5
													.replace('o', '9');//56 p5229 v52d5
	
		System.out.println("\nMe devuelve = " + strMurcielago);
		System.out.println("---------------------------------");
		
		/*-----------------------------------------------------*/
		//recorrer el String:
		/*-----------------------------------------------------*/
		
		for (int i=0; i<strTextoOriginal.length(); i++)
			System.out.println("\npos " + i + " char = " + strTextoOriginal.charAt(i));
		//me devuelve en cada posicion que letra hay.
		
	}

}
