package modulo6;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EjemploCalendar {

	public static void main(String[] args) {
		//presentar el tema de las fechas
				Date fechaHoy = new Date();
				
				//la otra clase para manerjar fechas
				//Calendar es una clase abstracta y no se pueden crear objetos
				//getIntance() es un metodo esttico //lo puedo utilizar necesitada de crear un objetos
				
				
				Calendar cal = Calendar.getInstance(); //patron de diseo singleton
				
				
				
				//nuestro tercer actor en la obtra
				SimpleDateFormat sdf = new SimpleDateFormat("EEEEEEEEE dd-MM-yyyy- HH:mm:ss");
				
				
				System.out.println("hoy es " + fechaHoy);
				System.out.println("hoy es " + sdf.format(fechaHoy));
				System.out.println("el numero del dia de hoy " + cal.get(Calendar.DATE));
				System.out.println("el mes del dia de hoy " + (cal.get(Calendar.MONTH)+1));
				System.out.println("el ao del dia de hoy " + cal.get(Calendar.YEAR));
				
				//si quiero saber el dis de hoy 
				switch (cal.get(Calendar.DAY_OF_WEEK)) {
				case Calendar.SUNDAY:
					System.out.println("hoy es domingo");
					break;
				case Calendar.MONDAY:
					System.out.println("hoy es lunes");
					break;

				case Calendar.TUESDAY:
					System.out.println("hoy es martes");
					break;
				case Calendar.WEDNESDAY:
					System.out.println("hoy es miercoles");
					break;
				case Calendar.THURSDAY:
					System.out.println("hoy es jueves");
					break;
					
				case Calendar.FRIDAY:
					System.out.println("hoy es viernes");
					break;
				case Calendar.SATURDAY:
					System.out.println("hoy es sabado");
					break;
				
				default:
					break;
				}
				
				
				//tambien puedo hacer operaiones
				System.out.println("\n\nla fecha de hoy  mas 10 dias");
				cal.add(Calendar.DATE, 10);
				System.out.println("la fecha de hoy mas 10 dias es " + sdf.format(cal.getTime()));
				
				cal.add(Calendar.MONTH, 3);
				System.out.println("y ahora....3 meses mas da " + sdf.format(cal.getTime()) );
				


	}

}
