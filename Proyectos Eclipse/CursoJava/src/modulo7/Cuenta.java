package modulo7;

public class Cuenta {

	//los atributos
	
	private int numero;
	private float saldo;
	
	
	//constructores :
	//es el primer m�todo que se ejecuta cuando se crea un objeto
	
	//cuando 2 m�todos tienen el mismo nombre y se diferencian con el tipo de par�metro
	//eso se lo llama sobrecarga; en este caso el constructor est� sobrecargado
	
	public Cuenta(int pNum, float pSaldo) {
		numero = pNum;
		saldo = pSaldo;
	}
	
	public Cuenta() {
		
	}


	//accesors
	//setters y getters

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	
	public int getNumero() {
		return numero;
	}
	
	public void setSaldo (float saldo) {
		this.saldo = saldo;
	}
	
	
	public float getSaldo() {
		return saldo;
	}
	
	//m�todos de negocios
	public void acreditar(float pMonto) {
		saldo += pMonto;
	}
	
	public void debitar(float pMonto) {
		saldo -= pMonto;
	}

//con control + o, veo la estructura de mi clase
	
	
	
	
	
	
	
}
