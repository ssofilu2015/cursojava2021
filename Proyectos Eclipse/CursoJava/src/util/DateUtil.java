package util;

import java.util.Calendar;
import java.util.Date;

/**
 * Esta clase tiene una serie de metodos vinculados con el manejo de fechas ofreciendo un conjuto de servicios
  */
public class DateUtil {
	
	/**
	 * Este metodo devuelve el a�o de la fecha que se envia por parametro
	 * @param pFecha corresponde al parametro donde se envia la fecha
	 * @return devuele un valor de tipo int donde se encuentra el 
	 */
	public static int getAnio(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);		
		return cal.get(Calendar.YEAR);
	}
	
	
	public static int getMes(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);		
		return cal.get(Calendar.MONTH)+1;
	}
	
	public static int getDia(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);		
		return cal.get(Calendar.DATE);
	}

}
