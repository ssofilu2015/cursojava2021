package utiltest;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.DateUtil;

class DateUtilTest {

	//lote de pruebas
		Date fechaCumple;


	@Before
	void setUp() throws Exception {
		//se ejecuta antes de cada prueba
		//creo mi lote de prueba
				Calendar cal = Calendar.getInstance();
				cal.set(1972, Calendar.NOVEMBER, 17);
				fechaCumple = cal.getTime();

	}

	@After
	void tearDown() throws Exception {
		//se ejecuta despes de cada prueba
				fechaCumple = null;

	}
	
	@Test
	public void testGetAnio1942() {
		fechaCumple.setYear(1942);
		assertEquals(1942, DateUtil.getAnio(fechaCumple));
	}
	



	@Test
	void testGetAnio() {
		assertEquals(1972, DateUtil.getAnio(fechaCumple));
	}
	
	@Test
	void testGetMes() {
		assertEquals(11, DateUtil.getMes(fechaCumple));
	}
	
	@Test
	void testGetDia() {
		assertEquals(4, DateUtil.getDia(fechaCumple));
	}

}


/* JUNIT es un framework (conjunto de clases)
 * @notations..., una marca
 * @before, se ejecuta antes de cada prueba
 * @after, se ejecuta despues de cada prueba
 * Garantiza un lote de pruebas limpio.
 * 
 * */
